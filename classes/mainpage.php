<?php
class mainpage
{
    private $db;

    function __construct($db)
    {
        $this->db = $db;
    }
    function getYear()
    {
        $sql = "SELECT year_id, year
                FROM public.year
                ";

        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $query = $stmt->fetchALL(PDO::FETCH_ASSOC);
        return $query;
    }
    function getSemester()
    {
        $sql = "SELECT semester_id, name
	            FROM public.semester    
                ";

        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $query = $stmt->fetchALL(PDO::FETCH_ASSOC);
        return $query;
    }
    function getChart($data)
    {
        $sql = "SELECT subject_id, name, score, year_id, semester_id
                FROM public.subject
                WHERE public.subject.year_id = :year AND public.subject.semester_id = :semester
  
                ";

        $stmt = $this->db->prepare($sql);
        $stmt->execute($data);
        $query = $stmt->fetchALL(PDO::FETCH_ASSOC);
        return $query;
    }
}
