<?php

use Psr\Container\ContainerInterface;

class mainpageController
{
    protected $container;

    // constructor receives container instance
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    function render_personpage($request, $response, $args)
    {
        // 登入介面
        session_destroy();
        $response = $this->container->view->render($response, "mainpage.html");
        return $response;
    }
    function login($request, $response, $args)
    {
        // login 取得登入者 id 
        $data = $request->getParsedBody();
        $user = new user($this->container->db);
        $result = $user->login($data);
        $response = $response->withHeader("Content-type", "application/json");
        $response = $response->withJson($result);
        return $response;
    }
    public function get_login($request, $response, $args)
    {
        // 跳轉到該使用者權限的網頁
        $number = $args['number'];
        if ($number == $_SESSION['id']) {
            $response = $this->container->view->render($response, "index.html");
        } else {
            $response = $response->withRedirect('/');
        }
        return $response;
    }
    public function getYear($request, $response, $args)
    {
        // 取得年份
        $mainpage = new mainpage($this->container->db);
        $result = $mainpage->getYear();
        $response = $response->withHeader("Content-type", "application/json");
        $response = $response->withJson($result);
        return $response;
    }
    public function getSemester($request, $response, $args)
    {
        // 取得學期
        $mainpage = new mainpage($this->container->db);
        $result = $mainpage->getSemester();
        $response = $response->withHeader("Content-type", "application/json");
        $response = $response->withJson($result);
        return $response;
    }
    public function getChart($request, $response, $args)
    {
        // 取得圖表
        $data = $request->getQueryParams();
        $mainpage = new mainpage($this->container->db);
        $result = $mainpage->getChart($data);
        $response = $response->withHeader("Content-type", "application/json");
        $response = $response->withJson($result);
        return $response;
    }
}
