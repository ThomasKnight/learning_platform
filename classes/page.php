<?php
class page
{
    private $db;

    function __construct($db)
    {
        $this->db = $db;
    }
    function getUserName($data)
    {
        // 從資料庫 public.user 的 table 裏面取出所有的 user_name
        $sql = "SELECT user_name
                FROM public.user
                WHERE public.user.user_id = :user_id
                ";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":user_id", $data['user_id'], PDO::PARAM_STR);
        $stmt->execute();
        $query = $stmt->fetchALL(PDO::FETCH_ASSOC);
        return $query;
    }
    function getCourse()
    {
        // 從資料庫 public.course 的 table 裏面取出所有的 course_id, course_name
        $sql = "SELECT course_id, course_name
                FROM public.course;
                ";

        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $query = $stmt->fetchALL(PDO::FETCH_ASSOC);
        return $query;
    }

    function getAnnouncement($data)
    {
        //從資料庫取得個科目公告
        if ($data["subject"] != 0) {
            $sql = "SELECT announcement_id , public.announcement.course_id , title , content , post_user , time_now 
                    FROM public.announcement
                    LEFT JOIN public.course
                    ON public.course.course_id = public.announcement.course_id
                    WHERE public.announcement.course_id = :subject
                    ORDER BY time_now DESC 
                    ";
            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(':subject', $data["subject"], PDO::PARAM_INT);
        } else {
            $sql = "SELECT announcement_id , public.announcement.course_id , title , content , post_user , time_now 
                    FROM public.announcement
                    LEFT JOIN public.course
                    ON public.course.course_id = public.announcement.course_id
                    ORDER BY time_now DESC 
                    ";
            $stmt = $this->db->prepare($sql);
        }
        $stmt->execute();
        $query = $stmt->fetchALL(PDO::FETCH_ASSOC);
        return $query;
    }

    function getMaterial($data)
    {
        if ($data["subject"] != 0) {
            $sql = "SELECT public.course_material.material_id, public.course_material.course_id, public.course_material.title, public.course.course_name, public.course_material.content, public.course_material.time_now
                    FROM public.course_material
                    LEFT JOIN public.course
                    ON public.course.course_id = public.course_material.course_id
                    WHERE public.course_material.course_id = :subject
                    ORDER BY 1
                ;";

            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(':subject', $data["subject"], PDO::PARAM_INT);
        } else {
            $sql = "SELECT public.course_material.material_id, public.course_material.course_id, public.course_material.title, public.course.course_name, public.course_material.content, public.course_material.time_now
                    FROM public.course_material
                    LEFT JOIN public.course
                    ON public.course.course_id = public.course_material.course_id
                    ORDER BY 1
                ;";
            $stmt = $this->db->prepare($sql);
        }

        $stmt->execute();
        $query = $stmt->fetchALL(PDO::FETCH_ASSOC);
        return $query;
    }
    function addMaterial($data)
    {
        $sql = "INSERT INTO public.course_material(course_id, title, content)
                VALUES (:course, :title, :content) 
            ;";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':course', $data["subject"], PDO::PARAM_INT);
        $stmt->bindValue(':title', $data["title"], PDO::PARAM_STR);
        $stmt->bindValue(':content', $data["newfile"], PDO::PARAM_STR);
        $stmt->execute();
        $query = $stmt->fetchALL(PDO::FETCH_ASSOC);
        return $query;
    }
}
