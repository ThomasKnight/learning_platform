<?php

use Psr\Container\ContainerInterface;

class pageController
{
    protected $container;

    // constructor receives container instance
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    public function getUserName($request, $response, $args)
    {
        // 取得使用者名稱
        $page = new page($this->container->db);
        $result = $page->getUserName(['user_id' => $_SESSION['id']]);
        $response = $response->withHeader("Content-type", "application/json");
        $response = $response->withJson($result);
        return $response;
    }
    public function getCourse($request, $response, $args)
    {
        // 取得各科科目
        $page = new page($this->container->db);
        $result = $page->getCourse();
        $response = $response->withHeader("Content-type", "application/json");
        $response = $response->withJson($result);
        return $response;
    }
    public function getAnnouncement($request, $response, $args)
    {
        //取得各科公告
        $page = new page($this->container->db);
        $data = $request->getQueryParams();
        $result = $page->getAnnouncement($data);
        $response = $response->withHeader("Content-type", "application/json");
        $response = $response->withJson($result);
        return $response;
    }
    public function getMaterial($request, $response, $args)
    {
        // 取得教材區內容
        $page = new page($this->container->db);
        $data = $request->getQueryParams();
        $result = $page->getMaterial($data);
        $response = $response->withHeader("Content-type", "application/json");
        $response = $response->withJson($result);
        return $response;
    }
    public function addMaterial($request, $response, $args)
    {
        // 上傳檔案
        $uploadedFiles = $request->getUploadedFiles();
        $newfile = $uploadedFiles['newfile'];
        // $data = $request->getParsedBody();
        // $datas = array();
        // foreach ($data as $key => $value) {
        //     $datas[$key] = $value;
        // }
        // var_dump($datas);
        if ($newfile->getError() === UPLOAD_ERR_OK) {
            $name = uniqid(date('Ymd') . '-');
            $name .= $newfile->getClientFilename();
            $whitelist = array('127.0.0.1', '::1');
            if (!in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
                $result = $newfile->moveTo("uploads/$name");
                $uploadedFiles['newfile'] = $name;
            } else {
                $result = $newfile->moveTo("uploads/$name");
                $uploadedFiles['newfile'] = $name;
            }
            // $response = $response->withHeader("Content-type", "application/json");
            // $response = $response->withJson($result);
            // return $response;
        }
        // 新增教材區內容
        $page = new page($this->container->db);
        $data = $request->getParsedBody();
        $datas = array();
        foreach ($uploadedFiles as $key => $value) {
            $datas[$key] = $value;
        }
        foreach ($data as $key => $value) {
            $datas[$key] = $value;
        }
        $result = $page->addMaterial($datas);
        $response = $response->withHeader("Content-type", "application/json");
        $response = $response->withJson($result);
        return $response;
    }
}
