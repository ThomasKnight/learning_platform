<?php
class user
{
    private $db;

    function __construct($db)
    {
        $this->db = $db;
    }
    function login($data)
    {
        $sql = "SELECT user_id, user_name
                FROM public.user
                WHERE public.user.account = :account AND public.user.password = :password";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":account", $data['account'], PDO::PARAM_STR);
        $stmt->bindValue(":password", $data['password'], PDO::PARAM_STR);
        $stmt->execute();
        $query = $stmt->fetchALL(PDO::FETCH_ASSOC);
        foreach ($query as $key => $id) {
            $_SESSION['id'] = $id['user_id'];
            break;
        }
        return $query;
    }
    // public function get_login($data)
    // {
    //     $sql = "SELECT public.user_role.user_id, public.user_role.role_id
    //             FROM public.user_role
    //             WHERE public.user_role.user_id = :user_id;";

    //     $stmt = $this->db->prepare($sql);
    //     $stmt->bindValue(':user_id', $data['user_id'], PDO::PARAM_STR);
    //     $stmt->execute();
    //     $query = $stmt->fetchALL(PDO::FETCH_ASSOC);
    //     foreach ($query as $key => $role) {
    //         return $role['role_id'];
    //     }
    // }
}
