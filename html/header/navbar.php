<?
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--npm-->
    <link rel="stylesheet" href="/node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/node_modules/bootstrap-icons/font/bootstrap-icons.css">
    <script src="/node_modules/jquery/dist/jquery.min.js" charset="utf-8"></script>
    <script src="/node_modules/popper.min.js"></script>
    <script src="/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/node_modules/boxicons/dist/boxicons.js"></script>
    <link rel="stylesheet" href="/css/navbar.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/DataTables/datatables.min.css"/>
    <script type="text/javascript" src="/node_modules/DataTables/datatables.min.js"></script>

</head>

<body>
    <header class="navbar navbar-expand-md navbar-dark bg-dark" role="navigation">
        <a class="navbar-brand" href="">
            <span class="h3 mx-1">Learning Platform</span>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/mainpage">個人主頁 <span class="sr-only"></span></a>
                </li>
                <li class="nav-item active">
                <a class="nav-link" href="">學習互動區 <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">校園廣場</a>
                </li>
            </ul>
            <li class="nav-item dropdown no-arrow">
                <a class="nav-link" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="form-inline">
                        <img class="img-profile rounded-circle" src="/picture/me.png" width="45" height="45">
                        <span class="ml-2 name">
                            <div id='login_name'></div>
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="/">
                        <box-icon class="" name='log-out'></box-icon>
                        <span class="text-center">登出</span>
                    </a>
                </div>
        </div>
    </header>
    <script>
        $(function() {
            // 取得使用者名稱
            $.ajax({
                url: "/billboard/getUserName",
                type: "GET",
                async: false,
                success: function(response) {
                    $(response).each(function() {
                        $("#login_name").html(`${this.user_name}`);
                    });
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest.status);
                }
            });
        });
    </script>
</body>

</html>