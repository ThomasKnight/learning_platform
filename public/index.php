<?php
session_start();

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Slim\Http\Response as Response;
use \Slim\Http\UploadedFile;

require '../vendor/autoload.php';

// Create and configure Slim app
$config['debug'] = true;
$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

$app = new \Slim\App(['settings' => $config]);

$container = $app->getContainer();

$container['upload_directory'] = __DIR__ . "/../uploads/";
$container['view'] = new \Slim\Views\PhpRenderer('../html/');

$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $pdo = new PDO('pgsql:host=140.127.74.174;port=5432;dbname=postgres;', 'postgres', 'pgsql');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};

// Define app routes
$app->group('', function () use ($app) {
    $app->get('/', \userController::class . ':render_login');
    $app->group('/login', function () use ($app) {
        $app->post('', \userController::class . ':login');
        $app->get('/{number}', \userController::class . ':get_login');
    });
    $app->group('/billboard', function () use ($app) {
        $app->get('', \pageController::class . ':render_index');
        $app->get('/data/{id}', \pageController::class . ':get_billboard');
        $app->get('/getCourse', \pageController::class . ':getCourse');
        $app->get('/getUserName', \pageController::class . ':getUserName');
        $app->get('/getAnnouncement', \pageController::class . ':getAnnouncement');
        $app->get('/getMaterial', \pageController::class . ':getMaterial');
        $app->post('/addMaterial', \pageController::class . ':addMaterial');
        // $app->post('/getUploadFiles', \pageController::class . ':getUploadFiles');
    });
    $app->group('/mainpage', function () use ($app) {
        $app->get('', \mainpageController::class . ':render_personpage');
        $app->get('/getYear', \mainpageController::class . ':getYear');
        $app->get('/getSemester', \mainpageController::class . ':getSemester');
        $app->get('/getChart', \mainpageController::class . ':getChart');
    });

    $app->get('/page', function (Request $request, Response $response, $args) {
        $response = $this->view->render($response, 'page.html');
        return $response;
    });
});

// Run app
$app->run();
